package nl.capgemini.testing.training.api.ra.restservice;

public record Greeting(long id, String content) { }